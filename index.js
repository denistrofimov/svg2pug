const html2pug = require('html2pug');
const fs = require('fs');
const path = require('path');
const mkdirP = require('mkdirp');
const argv = require('minimist')(process.argv.slice(2));
const o = argv.o || 'out';

crawling(process.cwd());

function crawling(dir) {
    fs.readdirSync(dir).forEach(f => {
        const file = `${dir}/${f}`;
        const stat = fs.lstatSync(file);
        if (stat.isDirectory()) {
            crawling(file);
        } else if (stat.isFile() && path.extname(file) === '.svg') {
            const output = path.join(o, path.relative(process.cwd(), dir), f.replace(/svg$/g, 'pug'));
            try {
                const html = fs.readFileSync(file).toString();
                const pug = html2pug(html, {tabs: true, fragment: true});
                const folder = path.dirname(output);
                mkdirP.sync(folder);
                fs.writeFileSync(output, pug);
            } catch (e) {
                console.log(e.message, file);
            }
        }
    });
}